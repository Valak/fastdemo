//
//  FastDemoApp.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI

@main
struct FastDemoApp: App {
    var body: some Scene {
        WindowGroup {
            SearchMovieView().environmentObject(MoviePlaylist())
        }
    }
}
