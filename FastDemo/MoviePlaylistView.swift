//
//  MoviePlaylistView.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI

struct MoviePlaylistView: View {
    
    @EnvironmentObject var playList: MoviePlaylist
    
    var body: some View {
        if playList.movies.isEmpty {
            Text("Your playlist is empty. Search for a movie, tap on the movie you'd like to add, and add it to your playlist.")
                .multilineTextAlignment(.leading)
                .padding(.horizontal, 15.0)
        }
        List(playList.movies) { movie in
            
            NavigationLink(destination: MovieDetailView(movie: movie, shouldHidePlaylistButton: true)) {
                SearchMovieRow(movie: movie)
            }
        }
        .navigationBarTitle(Text("Playlist"))
    }
    
}
