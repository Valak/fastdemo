//
//  MovieDetailView.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI

struct MovieDetailView: View {
    var movie: Movie

    @EnvironmentObject var playList: MoviePlaylist
    
    @State private var addedToPlaylist = false
    
    var shouldHidePlaylistButton: Bool = false
    
    var body: some View {
        
        ScrollView {
            ImageRow(model: movie).padding(/*@START_MENU_TOKEN@*/.horizontal, 50.0/*@END_MENU_TOKEN@*/).aspectRatio(contentMode: .fill)
            Text(movie.title)
                .font(.largeTitle)
                .multilineTextAlignment(.center)
                .padding(.horizontal, 50.0)
            Spacer()
                .padding(.leading)
            VStack {
                Text(movie.year)
                Spacer()
                if shouldHidePlaylistButton == false {
                    Button("Add To Playlist") {
                        self.playList.movies.append(movie)
                        addedToPlaylist = true
                        
                    }.disabled(addedToPlaylist)
                }
                
            }
            .padding(.bottom, 30.0)
        }
    }
}

struct MovieDetailView_Previews: PreviewProvider {
    static var previews: some View {
        MovieDetailView(movie: Movie(title: "Star Wars", year: "1994", imdbId: "298409", posterImage: "https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg"))
            .previewDevice("iPhone 11 Pro")
            .preferredColorScheme(.light)
    }
}
