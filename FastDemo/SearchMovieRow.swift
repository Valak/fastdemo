//
//  SearchMovieRow.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI

struct SearchMovieRow: View {
    
    @State var movie: Movie

    var body: some View {
        HStack {

            ImageRow(model: movie).frame(width: 50, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            VStack(alignment: .leading) {
                Text(movie.title)
                Text(movie.year)
            }
            Spacer()
            }
            .frame(height: 60)
    }
}
