//
//  SearchMovieViewModel.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI
import Combine

final class SearchMovieViewModel: ObservableObject {

    @Published var name = ""

    @Published private(set) var movies = [Movie]()

    @Published private(set) var movieImages = [Movie: UIImage]()
    
    let baseUrl = "http://www.omdbapi.com/"
    let appendedAPIKey = "&apikey=75724787"

    private var searchCancellable: Cancellable? {
        didSet { oldValue?.cancel() }
    }

    deinit {
        searchCancellable?.cancel()
    }

    func search() {
        guard !name.isEmpty else {
            return movies = []
        }
        
        var urlComponents = URLComponents(string: baseUrl)!
        urlComponents.queryItems = [
            URLQueryItem(name: "s", value: name),
            URLQueryItem(name: "apikey", value: "75724787")
        ]
        

        var request = URLRequest(url: urlComponents.url!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        searchCancellable = URLSession.shared.dataTaskPublisher(for: request)
            .map { $0.data }
            .decode(type: SearchMovieResponse.self, decoder: JSONDecoder())
            .map { $0.movies }
            .replaceError(with: [])
            .receive(on: RunLoop.main)
            .assign(to: \.movies, on: self)
        
    }
}
