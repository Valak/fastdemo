//
//  SearchMovieView.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI

struct SearchMovieView: View {
    @ObservedObject var viewModel = SearchMovieViewModel()
    @EnvironmentObject var playList: MoviePlaylist
    
    var body: some View {
        NavigationView {
            VStack {
                SearchMovieBar(text: $viewModel.name) {
                    self.viewModel.search()
                }
                
                if viewModel.movies.isEmpty {
                    ScrollView {
                        Text("Empty Search Result")
                    }
                } else {
                    List(viewModel.movies) { movie in
                        
                        NavigationLink(destination: MovieDetailView(movie: movie)) {
                            SearchMovieRow(movie: movie)
                        }
                        
                    }
                }
                
            }
            .navigationBarTitle(Text("Movies"))
            
            .navigationBarItems(trailing:
                                    NavigationLink(destination: MoviePlaylistView()) {
                                        Image("playlist").resizable()
                                            .frame(width: 30, height: 30, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                                    }
                                
            )
            
        }
        .phoneOnlyStackNavigationView()
    }
}

extension View {
    func phoneOnlyStackNavigationView() -> some View {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return AnyView(self.navigationViewStyle(StackNavigationViewStyle()))
        } else {
            return AnyView(self)
        }
    }
}

struct SearchMovieView_Previews: PreviewProvider {
    static var previews: some View {
        SearchMovieView().environmentObject(MoviePlaylist())
    }
}
