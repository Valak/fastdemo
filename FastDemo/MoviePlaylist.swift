//
//  MoviePlaylist.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//


import SwiftUI
class MoviePlaylist: ObservableObject {
    @Published var movies = [Movie]()
}
