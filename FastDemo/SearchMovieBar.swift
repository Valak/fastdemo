//
//  SearchMovieBar.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//
import SwiftUI

struct SearchMovieBar: View {
    
    @Binding var text: String
    @State var action: () -> Void
    @State private var showingAlert = false
    
    var body: some View {
        ZStack {
            Color.gray
            HStack {
                TextField("Search Movie", text: $text)
                    .padding([.leading, .trailing], 8)
                    .frame(height: 32)
                    .background(Color.white.opacity(0.4))
                    .cornerRadius(8)

                Button(
                    action: {
                        guard Reachability().isConnectedToNetwork() else {
                            self.showingAlert = true
                            return
                        }
                        self.showingAlert = false
                        action()
                    },
                    label: { Text("Search") }
                ).alert(isPresented: $showingAlert, content: {
                    Alert(title: Text("Offline"), message: Text("Your internet connection is offline"), dismissButton: .default(Text("Got it!")))
                })
                    .foregroundColor(Color.white)
                }
                .padding([.leading, .trailing], 16)
            }
            .frame(height: 64)
    }
}

