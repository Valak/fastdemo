//
//  ImageParser.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import SwiftUI

struct ImageRow: View {
    let model: Movie
    var body: some View {
        VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
            ImageViewContainer(imageURL: model.posterImage)
        })
    }
}

struct ImageViewContainer: View {
    
    @ObservedObject var remoteImageURL: RemoteImageURL
    
    init(imageURL: String) {
        
        remoteImageURL = RemoteImageURL(imageURL: imageURL)
    }
    
    var body: some View {
        Image(uiImage: ((remoteImageURL.data.isEmpty) ? UIImage(imageLiteralResourceName: "loading") : UIImage(data: remoteImageURL.data)) ?? UIImage(imageLiteralResourceName: "loading")).resizable()
    }
}

class RemoteImageURL: ObservableObject {
    
    @Published var data = Data()
    
    init(imageURL: String) {
        guard let url = URL(string: imageURL) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            
            DispatchQueue.main.async {
                self.data = data
            }
        }.resume()
    }
}

