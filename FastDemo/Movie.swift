//
//  Movie.swift
//  FastDemo
//
//  Created by Vala Kohnechi on 1/11/21.
//

import Foundation
import SwiftUI

struct Movie: Hashable, Identifiable, Decodable {
    var id: String { imdbId }
    var title: String
    var year: String
    var imdbId: String
    var posterImage: String
    
    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbId = "imdbID"
        case posterImage = "Poster"
    }
}

struct SearchMovieResponse: Decodable {
    var movies: [Movie]
    enum CodingKeys: String, CodingKey {
        case movies = "Search"
    }
}
